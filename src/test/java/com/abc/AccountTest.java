package com.abc;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

/**
 * Created by ASUS on 28-Feb-16.
 */
public class AccountTest {

    @Test
    public void testInterestEarned_savings() {
        Account account = new Account(Account.SAVINGS);
        double amount = account.interestEarned();
        assertEquals(amount, 0, 0);
    }

    @Test
    public void testInterestEarned_maxi() {
        Account account = new Account(Account.MAXI_SAVINGS);
        double amount = account.interestEarned();
        assertEquals(amount, 0, 0);
    }

}
